#include <iostream>

#include <fstream>
#include <sstream>

#include "plusaes.hpp"

// AES-CBC 256-bit
void cipher(std::vector<unsigned char> rawData, const std::vector<unsigned char> key, std::vector<unsigned char>& encrypted) {
  std::cout << "cipher init" << std::endl;

  // parameters
  const unsigned char iv[16] = {
      0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
      0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
  };

  // encrypt
  const unsigned long encrypted_size = plusaes::get_padded_encrypted_size(rawData.size());
  encrypted.resize(encrypted_size);

  plusaes::encrypt_cbc(&rawData[0], rawData.size(), &key[0], key.size(), &iv, &encrypted[0], encrypted.size(), true);

  std::cout << "cipher end" << std::endl;
}

void decipher(std::vector<unsigned char> encrypted, const std::vector<unsigned char> key, std::vector<unsigned char>& decrypted) {
  std::cout << "decipher init" << std::endl;

  const unsigned char iv[16] = {
      0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
      0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
  };

  unsigned long padded_size = 0;
  decrypted.resize(encrypted.size());

  plusaes::decrypt_cbc(&encrypted[0], encrypted.size(), &key[0], key.size(), &iv, &decrypted[0], decrypted.size(), &padded_size);

  std::cout << "decipher end" << std::endl;
}

void encrFile(std::string name) {

  // set file to idata
  std::ifstream ifile;
  ifile.open(name);
  if ( ! ifile.is_open()) {
    std::cout << "not found file" << std::endl;
    return;
  }

  std::string idata;
  std::ostringstream ss;
  if (ifile.good()) {
    ss << ifile.rdbuf();
    idata = ss.str();
  }


  // cipher idata
  std::vector<unsigned char> vecdata(idata.begin(), idata.end());

  std::vector<unsigned char> vecencr;

  std::string key = "12345678901234567890123456789012";

  const std::vector<unsigned char> veckey = plusaes::key_from_string((const char(*)[33])&key[0]); // 32-char = 256-bit

  cipher(vecdata, veckey, vecencr);

  // save file ciphered
  std::ofstream ofile;
  ofile.open("data");
  ofile << std::string(vecencr.begin(), vecencr.end());
  ofile.close();




}

void decrFile(std::string name) {

  // set file to idata
  std::ifstream ifile;
  ifile.open("data");
  if ( ! ifile.is_open()) {
    std::cout << "not found file" << std::endl;
    return;
  }

  std::string idata;
  std::ostringstream ss;
  if (ifile.good()) {
    ss << ifile.rdbuf();
    idata = ss.str();
  }


  // decipher
  std::vector<unsigned char> vecdata(idata.begin(), idata.end());

  std::string key = "12345678901234567890123456789012";
  const std::vector<unsigned char> veckey = plusaes::key_from_string((const char(*)[33])&key[0]); // 32-char = 256-bit

  std::vector<unsigned char> vecdecrypteddata;
  decipher(vecdata, veckey, vecdecrypteddata);


  // save file ciphered
  std::ofstream ofile;
  ofile.open(name);
  ofile << std::string(vecdecrypteddata.begin(), vecdecrypteddata.end());
  ofile.close();
}


int main(int argc, char** argv) {

  if (argc <= 2) {

    std::cout << "Add arguments" << std::endl;
    std::cout << "1 = enc | dec" << std::endl;
    std::cout << "2 = file to cipher | file to decipher" << std::endl;
    return 0; 
  }
  
  if (strcmp(argv[ 1 ], "enc") == 0) {
    encrFile(argv[ 2 ]);
    return 0;
  }

  if (strcmp(argv[ 1 ], "dec") == 0) {
    decrFile(argv[2]);
    return 0;
  }
}

