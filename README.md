# Cipher files from C++

## it uses AES 256 CBC to read and write files through a key


### How to compile
```
g++ main.cpp
```


### how to run
```sh
# this read image.png and generate data (cipher file)
./a.exe enc image.png

# this read data (cipher file) and generate image.png
./a.exe dec image.png
```


### 
It uses simple header file to manage AES cipher algorithm [`plusaes`](https://github.com/kkAyataka/plusaes)
